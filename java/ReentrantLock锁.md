# ReentrantLock锁

Java除了使用关键字Synchronized外，还可以使用ReentrantLock实现独占锁的功能，ReentrantLock比synchronized功能更加丰富，使用起来更加灵活，更适合复杂的并发场景。

### 与synchronized作比较

- synchronized是独占锁，加锁和解锁的过程都是自动完成，易与操作，但是不够灵活。ReentrantLock也是独占锁，加锁和解锁的过程需要手动执行，不易操作，但是更加灵活。

- synchronized和ReentrantLock都是可重入，不过synchronized是自动加锁和解锁，不用担心是否释放锁，ReentrantLock是手动加锁和解锁，而且次数要一样，不然其他线程无法获得锁。

- synchronized不可响应中断，一个线程获取不到锁就一直处于等待；ReentrantLock可以响应中断。

- ReentrantLock和synchronized最大的区别也是synchronized不具有的就是可实现公平锁机制。也就是在锁上等待时间最长的线程将首先获得锁的使用权，可以理解为谁排队时间长谁先获得锁。

  ### AQS

  Java Juc包中的工具实现的核心都是AQS,要想理解ReentrantLock的实现原理，需要先了解AQS以及AQS与ReentrantLock的关系。

  AQS,队列同步器，在juc包中的工具类都是依赖于AQS来实现同步控制。

![<u>AQS结构图</u>](https://tva1.sinaimg.cn/large/008i3skNly1grwxevilc7j31540k678x.jpg)

同步控制主要使用的信息如上图所示。AQS可以被当做是一个同步器的实现，并且具有排队的功能。当线程尝试获取AQS的锁时，如果AQS已经被别的线程占用锁，那么就会新建一个Node节点，并且加入到AQS的等待队列中，这个队列也是由AQS本身维护。当锁被释放时就会唤醒下一个节点尝试获取锁。

- 变量exclusiveOwnerThread互斥模式下，表示当前持有锁的线程

- 变量tail指向等待获取AQS的锁节点队列的最后一个节点

- 变量head指向队列中head节点，head节点的信息为空，不表示任何等待队列的线程。

- 变量state表示AQS同步器的状态，在不同的场景下含义也不一样，在ReentrantLock中表示AQS锁是否被占用，若为0则为没有占用，其他线程可以获取锁，若>=1则表示锁已经被获取，当大于时则表示同一线程多次重入锁。

  #### Node结构

  Node节点是AQS管理的等待队列的节点元素，除了head节点外，队列中的其他一个节点就代表一个正在等待获取锁的线程节点，Node参数有以下几个

  - $prev$:前置节点
  - next 后置节点
  - thread 等待获取锁的线程
  - waitStatus 节点的等待状态
    1. 为1表示节点已经取消，线程可能中断，不需要再等待获取锁了，在后续的执行过程中会跳过watistatus等于1的节点。
    2. -1表示当前节点的后置节点代表的线程需要被挂起
    3. -2表示当前线程正在等待的是**Condition锁**

#### reentrantLock与AQS的关系

![关系图](https://tva1.sinaimg.cn/large/008i3skNly1grwxiyn767j31000i8adv.jpg)

ReentrantLock中的两个核心内部类FairSync和NonfairSync都是通过继承AbstractQueuedSynchronizer类实现的公平锁和非公平锁，在ReentrantLock中最终的lock和unlock操作，最终都是由FairSync和NonfairSync来完成。

### ReentrantLock简单使用

#### 公平锁：等待时间越长，越先获取锁，排队功能

```java
public class FairLock {

    /**
     * 参数为true 调用公平锁构造方法
     */
    public static final Lock fairLock = new ReentrantLock(true);

    public static void main(String[] args) {
        new Thread(() -> getLock(), "test1").start();
        new Thread(() -> getLock(), "test2").start();
        new Thread(() -> getLock(), "test3").start();
        new Thread(() -> getLock(), "test4").start();
        new Thread(() -> getLock(), "test5").start();
        new Thread(() -> getLock(), "test6").start();
        new Thread(() -> getLock(), "test7").start();
        new Thread(() -> getLock(), "test8").start();
    }

    /**
     * 获取锁
     */
    public static void getLock() {
        for (int i = 0; i < 2; i++) {
            fairLock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "获得锁");
                TimeUnit.SECONDS.sleep(2);
            }catch (Exception e) {
                e.printStackTrace();
            }finally {
                fairLock.unlock();
            }

        }
    }
}
```

运行截图：

![运行截图](https://tva1.sinaimg.cn/large/008i3skNly1grwym6rk0cj30hw0he765.jpg)

**两次加锁和解锁的过程，体现了ReentrantLock可重入，两次获取锁的线程顺序一致，实现了公平锁。**

作为对比将“true"参数去掉，就是非公平锁的情况：

![](https://tva1.sinaimg.cn/large/008i3skNly1grwys3kddyj30bo0gsgn9.jpg)

### ReentrantLock底层原理

- #### NonfairSync#lock

```java
/**
*lock.lock() 程序入口
*/
final void lock() {
    if (compareAndSetState(0, 1))//【step1】
        setExclusiveOwnerThread(Thread.currentThread());//【step2】
    else
        acquire(1);//【step3】
}
```

step1】上面有提到，NonfairSync继承自AbstractQueuedSynchronizer，NonfairSync就是一个AQS，因此在步骤【1】其实就是利用CAS（一个原子性的比较并设置操作）尝试设置AQS的state为1。如果设置成功，表示获取锁成功；如果设置失败，表示state之前已经是>=1，已经被别的线程占用了AQS的锁，所示无法设置state为1，稍后会把线程加入到等待队列。

**非公平锁与公平锁：**对于NonfairSync非公平锁来说，线程只要执行lock请求，就会立马尝试获取锁，不会管AQS当前管理的等待队列中有没有正在等待的线程，这种操作是不公平的，没有先来后到；而稍后介绍的FairSync公平锁，则会在lock请求进行时，先判断AQS管理的等待队列中是否已经有正在等待的线程，有的话就是不尝试获取锁，直接进入等待队列，保证了公平性。

这一步需要熟悉的是CAS操作，分析一下compareAndSetState源码，如下。这一步利用unsafe包的cas操作，unsafe包类似一种java留给开发者的后门，可以用来直接操作内存数据，并且保证这个操作的原子性。在下面的代码中，stateOffset表示state比变量的内存偏移地址，用来寻找state变量内存位置。整个cas操作就是找到内存中当前的state变量值，并且与expect期待值比较，如果跟期待值相同，那么表示这个值是可以修改的，此时就会对state值进行更新；如果与期待值不一样，那么将不能进行更新操作。unsafe保证了比较与设置值的过程是原子性的，在这一步不会出现线程安全问题。

```JAVA
/**
* compareAndSetState()
*/
protected final boolean compareAndSetState(int expect, int update) {
    // See below for intrinsics setup to support this
    return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
}
```

【step2】操作是在设置state成功之后，表示当前线程获取AQS锁成功，需要设置AQS中的变量exclusiveOwnerThread为当前持有锁的线程，做保存记录。

【step3】当尝试获取锁失败的时候，就需要进行步骤3，执行acquire,进行再次尝试或者线程进入等待队列。

- #### AbstractQueuedSynchronizer#acquire

当第一次尝试获取锁失败之后，执行【step3】acquire方法，这个方法可以将一个尝试获取锁失败的线程放入AQS管理的等待队列进行等待，并且将线程挂起。实现逻辑在AbstractQueuedSynchronizer实现，NonfairSync通过继承AbstractQueuedSynchronizer获得等待队列管理的功能。

```java
public final void acquire(int arg) {
    if (!tryAcquire(arg) &&
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
        selfInterrupt();
}
```

- #### NonfairSync#tryAcquire---锁重入实现

```java
final boolean nonfairTryAcquire(int acquires) {
    final Thread current = Thread.currentThread();
    int c = getState();
    if (c == 0) {
        //如果此时state已经变为1，再次尝试一次获取锁
        if (compareAndSetState(0, acquires)) {
            setExclusiveOwnerThread(current);
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) {
        //否则判断当前持有AQS的锁的线程是不是当前请求获取锁的线程，是的话就进行锁重入。
        int nextc = c + acquires;
        if (nextc < 0) // overflow
            throw new Error("Maximum lock count exceeded");
        setState(nextc);
        return true;
    }
    return false;
}
```

对于NonfairSync#tryAcquire的实现，首先是重新尝试一次获取锁，如果还是获取不到，就尝试判断当前的是不是属于重入锁的情形。

对于重入锁的情形，就需要对state进行累加1，意思就是重入一次就对state加1。这样子，在解锁的时候，每次unlock就对state减一，等到state的值为0的时候，才能唤醒下一个等待线程。

如果获取成功，返回true；否则返回false，继续执行下一步acquireQueued(addWaiter(Node.EXCLUSIVE), arg))，添加一个Node节点进入AQS管理的等待队列。

- #### AbstractQueuedSynchronizer#addWaiter–添加Node到等待队列

  这个方法属于队列管理，也是由AbstractQueuedSynchronizer默认实现，NonfairSync继承获得。

  ```java
  private Node addWaiter(Node mode) {
      //新建一个Node节点，mode传入表示当前线程之间竞争是互斥模式
      Node node = new Node(Thread.currentThread(), mode);
      // Try the fast path of enq; backup to full enq on failure
      //尝试添加当前新建Node到链表队列末尾
      Node pred = tail;
      if (pred != null) {
          node.prev = pred;
          //利用cas设置尾指针指向的节点为当前线新建节点
          if (compareAndSetTail(pred, node)) {
              pred.next = node;
              return node;
          }
      }
      //当前是空的没有任何正在等待的线程Node的时候，执行enq(node)，初始化等待队列
      enq(node);
      return node;
  }
  ```

  ```java
  /**
  * 初始化等待队列
  */
  private Node enq(final Node node) {
      for (;;) {
          Node t = tail;
          if (t == null) { // Must initialize
              //新建一个空的头节点
              if (compareAndSetHead(new Node()))
                  tail = head;
          } else {
              //跟之前一样，利用cas这只当前新建节点为尾节点
              node.prev = t;
              if (compareAndSetTail(t, node)) {
                  t.next = node;
                  return t;
              }
          }
      }
  }
  ```

  参考文献：https://tech.meituan.com/2019/12/05/aqs-theory-and-apply.html