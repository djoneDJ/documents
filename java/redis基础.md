# redis

> redis是单线程的!

redis是很快的，redis是基于内存操作的，CPU不是redis的性能瓶颈，redis的瓶颈是根据机器的内存和网络带宽，既然可以使用单线程那就使用单线程。

> 为什么redis单线程还这么快？

redis是C语言编写的，官方提供的数据QPS 10万＋，完全不比使用k-v的memecache差

Redis将所有数据放入到内存中，使用单线程效率就是这么快，多线程涉及到CPU上下文的切换，这是耗时的操作，对于内存系统来说，没有上下文的切换效率是最高的。多次读写都是在一个CPU上进行的，所以在基于内存的情况下，单线程就是最佳的方案！



> redis基本命令（redis官网可查）

```bash
127.0.0.1:6379> keys *  # 查看所以的key
(empty array)
127.0.0.1:6379> set name dj # set一个key
OK
127.0.0.1:6379> set age 20
OK
127.0.0.1:6379> keys *
1) "name"
2) "age"
127.0.0.1:6379> exists name  # 判断当前的key是否存在
(integer) 1
127.0.0.1:6379> EXPIRE name 10 # 设置key的过期时间，单位是秒
(integer) 1
127.0.0.1:6379> ttl name
(integer) 4
127.0.0.1:6379> ttl name
(integer) 2
127.0.0.1:6379> ttl name
(integer) 1
127.0.0.1:6379> ttl name
(integer) -2
127.0.0.1:6379> get name
(nil)
127.0.0.1:6379> 
127.0.0.1:6379> move age 1 # 移除key
(integer) 1
127.0.0.1:6379> get age
(nil)
127.0.0.1:6379> type name #查看key的类型
string
127.0.0.1:6379> 
127.0.0.1:6379> append name jian # 拼接字符串，如果当前key不存在就相当于set key
(integer) 8
127.0.0.1:6379> get name
"dongjian"
127.0.0.1:6379> strlen name #查看字符串长度
(integer) 8
########################################################
# i++
127.0.0.1:6379> set views 0
OK
127.0.0.1:6379> get views
"0"
127.0.0.1:6379> incr views # 自增加1
(integer) 1
127.0.0.1:6379> decr views #自增减一
(integer) 0
127.0.0.1:6379> incrby views 10 #指定自增量步长
(integer) 10
127.0.0.1:6379> decrby views 10 #指定自减量步长
(integer) 0
#####################################################
# “dongjian”
127.0.0.1:6379> getrange name 0 3 #截取字符串
"dong"
127.0.0.1:6379> getrange name 0 -1 #截取全部字符串 相当于get
"dongjian"
127.0.0.1:6379> setrange name 1 xx #替换指定位置的字符串
(integer) 8
127.0.0.1:6379> get name
"dxxgjian"
######################################################
# setex (set with expire) # 设置字符串值并设置过期时间
# setnx (set if not exists) # 不存在设置，分布式锁中使用

127.0.0.1:6379> setex name 10 dongjian # 设置name 过期时间为10s
OK
127.0.0.1:6379> get name
"dongjian"
127.0.0.1:6379> setnx name1 dongjian1 # 设置name1为dongjian1，如果name1存在则设置失败
(integer) 1
127.0.0.1:6379> get name1
"dongjian"
Last login: Sat Aug 14 16:31:11 on ttys000
dongjian@dongjiandeMacBook-Pro ~ % docker exec -it cbaf96231a0e10c460c5947ee6e4e8e0551ce0e2c711b1192ca5b37fb23d12a5 /bin/sh
# redis-cli -p 6379
127.0.0.1:6379> ping 
PONG
127.0.0.1:6379> keys *
1) "name"
2) "name1"
127.0.0.1:6379> flushdb
OK
127.0.0.1:6379> keys *
(empty array)

127.0.0.1:6379> mset k1 v1 k2 v2 k3 v3 # 设置多个值
OK
127.0.0.1:6379> mget
(error) ERR wrong number of arguments for 'mget' command
127.0.0.1:6379> mget k1 k2 k3  # 获取多个key值
1) "v1"
2) "v2"
3) "v3"
127.0.0.1:6379> msetnx k1 v1 k4 v4 # msetnx 原子操作，要么成功，要么失败 set if not exists
(integer) 0
127.0.0.1:6379> 
# 设置对象
# set user:1 {name:zhangsan,age:3}
# 这是key的一个巧妙的设计，user:{id}:{field1,field2}，这样的设计在redis中是完全ok的
127.0.0.1:6379> mset user:1:name zhangsan user:1:age 20 # 设置 对象user的属性 user:1:name 和 user:1:age  
OK
127.0.0.1:6379> mget user:1
1) (nil)
127.0.0.1:6379> mget user:1:name user:1:age
1) "zhangsan"
2) "20"
########################################################################
# getset 先get再set
127.0.0.1:6379> getset db redis # 如果不存在值，则返回nil，设置新的值
(nil)
127.0.0.1:6379> get db
"redis"
127.0.0.1:6379> getset db mongodb # 如果存在值，则返回旧的值，并设置新的值
"redis"
127.0.0.1:6379> get db
"mongodb"
127.0.0.1:6379> 











```

## Redis数据类型

### String

命令如上！🔼

String类似的场景：value除了可以是我们的字符串还可以是数字类型，还可以用来做

- 计数器
- 统计多单位的数量（浏览量 播放量..）
- 粉丝数

### List

在redis中，list可以玩成 堆、栈、队列、阻塞队列

在redis中list命令都是l开头

```bash
127.0.0.1:6379> lpush list one  # lpush:将一个或者多个值添加到list列表的头部,返回值是元素在list中的位置，将
(integer) 1
127.0.0.1:6379> LPUSH list two
(integer) 2
127.0.0.1:6379> LRANGE list 0 -1 # lrange:查询list值（0 -1获取整个值）
1) "two"
2) "one"
127.0.0.1:6379> LRANGE list 0 1 # lrange:查询list值（带偏移量）元素是倒着输出的
1) "two"
2) "one"

127.0.0.1:6379> RPUSH list three # 将一个或多个值添加到list列表的尾部
(integer) 3
127.0.0.1:6379> lrange list 0 -1
1) "two"
2) "one"
3) "three" # 尾部
127.0.0.1:6379> 
####################################################################
# 移除值 POP:lpop、rpop
127.0.0.1:6379> LPOP list # 移除列表左边的一个元素，返回是移除的值
"two"
127.0.0.1:6379> RPOP LIS # 移除列表右边的一个元素，返回是移除的值
"three"
127.0.0.1:6379> lrange list 0 -1
1) "one"
127.0.0.1:6379> 
###############################################################
# LINDEX 通过元素下标获取元素值
127.0.0.1:6379> LINDEX list 2 # 通过下标获取某个元素的值
"one"
127.0.0.1:6379> LINDEX list 0
"three"
###############################################################
# Llen 获取列表长度
127.0.0.1:6379> Llen list
(integer) 3
###############################################################
# Lrem 移除指定的值 取关：userId
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
4) "one"
127.0.0.1:6379> 
127.0.0.1:6379> LREM list 1 one # 移除列表中1个one值
(integer) 1
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
127.0.0.1:6379> LREM list 2 three # 移除列表中2个three值
(integer) 2
127.0.0.1:6379> lrange list 0 -1
1) "two"
127.0.0.1:6379> 
###############################################################
# Ltrim:修剪，截取元素
127.0.0.1:6379> LPUSH list one 
(integer) 1
127.0.0.1:6379> LPUSH list two
(integer) 2
127.0.0.1:6379> LPUSH list three
(integer) 3
127.0.0.1:6379> RPUSH list four
(integer) 4
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "two"
3) "one"
4) "four"
127.0.0.1:6379> LTRIM list 1 2 # 通过下标截取元素，截取【1，2】元素，这个list被改变了，剩下截断后的元素
OK
127.0.0.1:6379> LRANGE list 0 -1
1) "two"
2) "one"
127.0.0.1:6379> 
##############################################################
# rpoplpush 移除列表中的最后一个元素值并把它添加到另外一个列表的标头
127.0.0.1:6379> RPUSH mylist one
(integer) 1
127.0.0.1:6379> RPUSH mylist two
(integer) 2
127.0.0.1:6379> RPUSH mylist three
(integer) 3
127.0.0.1:6379> LRANGE mylist 0 -1
1) "one"
2) "two"
3) "three"
127.0.0.1:6379> RPOPLPUSH mylist myotherlist # 移除mylist列表中尾部最后一个值，并把这个元素添加到myotherlist列表的头部
"three"
127.0.0.1:6379> LRANGE mylist 0 -1 # 查看原来的列表，元素移除成功
1) "one"
2) "two"
127.0.0.1:6379> LRANGE myotherlist 0 -1 # 查看当前的列表，元素是存在的，元素添加成功
1) "three"
127.0.0.1:6379> 
############################################################
# lset 更新列表指定下标的值
127.0.0.1:6379> EXISTS list # 判断列表是否存在
(integer) 0
127.0.0.1:6379> EXISTS mylist
(integer) 1
127.0.0.1:6379> LRANGE mylist 0 -1
1) "one"
2) "two"
127.0.0.1:6379> lset list 0 v1 # 更新指定下标的值，如果不存在，则报错
(error) ERR no such key
127.0.0.1:6379> lset mylist 0 v1 # 更新指定下标的值，如果存在，则更新值
OK
127.0.0.1:6379> lrange mylist 0 -1
1) "v1"
2) "two"
127.0.0.1:6379> 
############################################################
linsert 将某一个具体的值插入到列表某个元素的前/后位置
127.0.0.1:6379> RPUSH list hello
(integer) 1
127.0.0.1:6379> RPUSH list wo rld
(integer) 3
127.0.0.1:6379> LRANGE list 0 -1
1) "hello"
2) "wo"
3) "rld"
127.0.0.1:6379> LINSERT list before wo new  # 在wo元素前面插入一个new元素
(integer) 4
127.0.0.1:6379> LRANGE list 0 -1
1) "hello"
2) "new"
3) "wo"
4) "rld"
127.0.0.1:6379> LINSERT list after wo old # 在wo元素后面插入一个old元素
(integer) 5
127.0.0.1:6379> LRANGE list 0 -1
1) "hello"
2) "new"
3) "wo"
4) "old"
5) "rld"
127.0.0.1:6379> 
```

> list小结

它实际是一个链表，before Node after, left,right都可以进行元素插入。

- 如果key不存在，则创建新的链表
- 如果key存在，则新增元素
- 如果移除了所值，空链表，也代表链表不存在
- 在两边插入和修改元素，效率最高，中间元素，效率相对低一点。

使用场景：消息排队 消息队列（Lpush，Rpop）栈（Lpush，Lpop）

### set

> 命令

```bash
127.0.0.1:6379> sadd myset s1 s2 # 向set中添加元素，set是无序不重复集合
(integer) 2
127.0.0.1:6379> SMEMBERS myset # 查询set元素
1) "s1"
2) "s2"
127.0.0.1:6379> SISMEMBER myset s1 # 判断set元素是否存在
(integer) 1
127.0.0.1:6379> 
127.0.0.1:6379> SCARD myset # 获取set中的元素个数
(integer) 2
127.0.0.1:6379> srem myset s1 # 移除set中的元素
(integer) 1
127.0.0.1:6379> SCARD myset
(integer) 1
127.0.0.1:6379> 
###################################################
# sranmember 随意抽取元素，抽奖！
127.0.0.1:6379> SADD myset s2 s3 s4
(integer) 2
127.0.0.1:6379> SRANDMEMBER myset # 随机取出一个元素
"s2"
127.0.0.1:6379> SRANDMEMBER myset 2 # 随机取出两个元素
1) "s4"
2) "s3"
127.0.0.1:6379> SRANDMEMBER myset 2
1) "s2"
2) "s3"
127.0.0.1:6379> SRANDMEMBER myset 2
1) "s4"
2) "s3"
127.0.0.1:6379> 
###################################################
# spop 随机移除一个元素！
127.0.0.1:6379> SPOP myset
"s4"
127.0.0.1:6379> SMEMBERS myset
1) "s2"
2) "s3"
127.0.0.1:6379> 
###################################################
# 将一个指定元素从一个set集合移入另外一个集合
127.0.0.1:6379> sadd myset s1 s2 s3
(integer) 3
127.0.0.1:6379> SMEMBERS myset
1) "s1"
2) "s2"
3) "s3"
127.0.0.1:6379> sadd myset2 dongjian
(integer) 1
127.0.0.1:6379> SMEMBERS myset2
1) "dongjian"
127.0.0.1:6379> SMOVE myset myset2 s1 # 将元素s1从集合myset移入 集合myset2
(integer) 1
127.0.0.1:6379> SMEMBERS myset2
1) "s1"
2) "dongjian"
127.0.0.1:6379> 
###################################################
# 微博 B站共同关注！
127.0.0.1:6379> sadd set1 s1 s2 s3
(integer) 3
127.0.0.1:6379> sadd set2 s2 s4 s5
(integer) 3
127.0.0.1:6379> SDIFF set1 set2 # 差集
1) "s1"
2) "s3"
127.0.0.1:6379> SINTER set1 set2 # 交集 共同好友就可以这样实现！
1) "s2"
127.0.0.1:6379> SUNION set1 set2 
1) "s2"
2) "s3"
3) "s4"
4) "s1"
5) "s5"
127.0.0.1:6379> 

```

### hash（哈希）

map集合 key-map(k-v) 是一个map集合，hash本质和String没有太大区别，只是key-value

> 基础命令

```bash
127.0.0.1:6379> hset myhash field1 dongjian # 设置一个hash为myhash，并设置field1属性值为dongjian
(integer) 1
127.0.0.1:6379> hget myhash field1  # 查询myhash的field1属性值
"dongjian"
127.0.0.1:6379> HMSET myhash field1 hello field2 world # 设置多个属性
OK
127.0.0.1:6379> hmget myhash field1 field2 # 查询多个属性
1) "hello"
2) "world"q
127.0.0.1:6379> hgetall myhash # 查看所有属性
1) "field1"
2) "hello"
3) "field2"
4) "world"
127.0.0.1:6379> 

127.0.0.1:6379> HDEL myhash field1 # 删除hash指定字段！对应的value自动删除
(integer) 1
127.0.0.1:6379> HGETALL myhash
1) "field2"
2) "world"
127.0.0.1:6379> hset myhash field3 hi
(integer) 1
127.0.0.1:6379> HLEN myhash # 获取hash内容长度
(integer) 2
127.0.0.1:6379> 
c
# hexists 判断指定的hash字段是否存在
127.0.0.1:6379> HGETALL myhash
1) "field2"
2) "world"
3) "field3"
4) "hi"
127.0.0.1:6379> HEXISTS myhash field1 # 判断field1是否存在 不存在返回0
(integer) 0
127.0.0.1:6379> HEXISTS myhash field2 # 判断指定的field2是否存在，存在返回1
(integer) 1
127.0.0.1:6379> 
################################################################
# 只获取所有的field ->hkeys
# 只获取所有的values -> hvals
127.0.0.1:6379> HKEYS myhash # 获取所有的字段
1) "field2"
2) "field3"
127.0.0.1:6379> HVALS myhash # 获取所有的值
1) "world"
2) "hi"
127.0.0.1:6379> 
127.0.0.1:6379> HSETNX myhash field1 3 # 如果存在则不能设置，返回0
(integer) 0
127.0.0.1:6379> HSETNX myhash field2 4 # 如果不存在则能够设置，返回1
(integer) 1
```

使用场景：hash存储变更数据，用户信息的存储，user:userId name name1 age agenumber，存储经常变动的数据，hash更适合对象的存储，key为属性，value为属性值，与对象属性和属性值一一对应，String更适合字符串的存储！

### ZSET(有序集合)

在set的基础上添加一个值，增加排序功能

```bash
127.0.0.1:6379> ZADD myset 0 z1 1 z2 # 添加多个值
(integer) 2
127.0.0.1:6379> ZRANGE myset 0 -1 # 查询zset值
1) "z1"
2) "z2"
127.0.0.1:6379> 
################################################################
# zrangebyscores 排序功能
127.0.0.1:6379> zadd salary 1000 zhangsan
(integer) 1
127.0.0.1:6379> zadd salary 2000 lisi 3000 dongjian
(integer) 2
127.0.0.1:6379> ZRANDMEMBER salary  # 随机查找一个元素
"lisi"
127.0.0.1:6379> ZRANGEBYSCORE salary -inf inf withscores # 根据scores排序,范围是-inf~inf 负无穷大~正无穷大
1) "zhangsan"
2) "1000"
3) "lisi"
4) "2000"
5) "dongjian"
6) "3000"
127.0.0.1:6379> ZRANGEBYSCORE salary (1000 2000 withscores # 工资在（1000，2000】的升序排列
1) "lisi"
2) "2000"
127.0.0.1:6379> zrange salary 0 -1 withscores
1) "zhangsan"
2) "1000"
3) "lisi"
4) "2000"
5) "dongjian"
6) "3000"
127.0.0.1:6379> ZREVRANGE salary 0 -1 withscores # 从大到小降序排列
1) "dongjian"
2) "3000"
3) "lisi"
4) "2000"
5) "zhangsan"
6) "1000"
127.0.0.1:6379> 
################################################################
# zrem 移除元素
127.0.0.1:6379> zrange myset 0 -1
1) "z1"
2) "z2"
127.0.0.1:6379> zrem myset z1 # 移除几个myset中的z1元素
(integer) 1
127.0.0.1:6379> zrange myset 0 -1
1) "z2"
127.0.0.1:6379> 
################################################################
127.0.0.1:6379> ZADD myset 1 zhangsan 2 lisi 3 wangwu
(integer) 3
127.0.0.1:6379> ZCOUNT myset 1 3 # 统计指定区间的个数
(integer) 3
127.0.0.1:6379> ZCOUNT myset 1 2
(integer) 2
127.0.0.1:6379> 
```

案例：存储班级成绩表，工资表

普通消息 1，重要消息 2，通过zset进行加权

排行榜 Top N 定时刷新。

## 三大特殊数据类型

###  geospatial 地理位置

redis的Geo在redis 3.2版本就推出了，这个功能可以实现朋友的定位，附近的人、打车距离的计算等场景

geo命令：http://redis.cn/commands/geoadd.html

> 官方文档 https://www.redis.net.cn/order/3687.html

有效的经度从-180度到180度。
有效的纬度从-85.05112878度到85.05112878度。
当坐标位置超出上述指定范围时，该命令将会返回一个错误

查询地理位置的经纬度：https://jingweidu.bmcx.com/

> geoadd 添加地理位置

```bash
# geoadd 添加地理位置
# 南北极无法直接添加，我们一般下载城市数据，通过程序一次性导入
# 参数：纬度、经度、名称
127.0.0.1:6379> GEOADD china:city 116.23 40.22 beijing # 添加指定城市的经纬度
(integer) 1
127.0.0.1:6379> GEOADD china:city 121.48941 31.40527 shanghai
(integer) 1
127.0.0.1:6379> GEOADD china:city 113.27324 23.15792 guangzhou
(integer) 1
# 获取定位，返回的是坐标值
127.0.0.1:6379> GEOPOS china:city beijing # 从指定的key中获取城市的经纬度
1) 1) "116.23000055551528931"
   2) "40.2200010338739844"
127.0.0.1:6379> GEOPOS china:city beijing shanghai
1) 1) "116.23000055551528931"
   2) "40.2200010338739844"
2) 1) "121.48941010236740112"
   2) "31.40526993848380499"
```

> GEODIST 计算距离

两人之间的距离！

如果两个位置之间的其中一个不存在， 那么命令返回空值。

指定单位的参数 unit 必须是以下单位的其中一个：

- **m** 表示单位为米。
- **km** 表示单位为千米。
- **mi** 表示单位为英里。
- **ft** 表示单位为英尺。

如果用户没有显式地指定单位参数， 那么 `GEODIST` 默认使用米作为单位。

``` bash
127.0.0.1:6379> GEODIST china:city beijing shanghai # beijing 和 shanghai之间的直线距离
"1088618.6189"
127.0.0.1:6379> GEODIST china:city beijing shanghai km # 距离单位为km
"1088.6186"
127.0.0.1:6379> 
```

附近的人？（获取所有附近的人的地址，定位）以某一地址为圆心，半径内的范围

获取定位 geopos 

计算距离：geodist

获取附近的人：georadius

计数：count

```bash
127.0.0.1:6379> GEORADIUS china:city 110 30 10000 km withdist withcoord count 3 asc # 获取指定范围内附近的人，110 30当前定位，10000km 半径距离，count返回3人 asc/desc 由远到近/由近到远排序
1) 1) "guangzhou"
   2) "827.6084"
   3) 1) "113.27324062585830688"
      2) "23.1579209662846921"
2) 1) "shanghai"
   2) "1109.3250"
   3) 1) "121.48941010236740112"
      2) "31.40526993848380499"
3) 1) "beijing"
   2) "1269.3568"
   3) 1) "116.23000055551528931"
      2) "40.2200010338739844"
127.0.0.1:6379> 
```

> GEORADIUSBYMEMBER

找出位于指定范围内的元素，中心点是由给定的位置元素决定。

``` bash
# 找出指定元素周围的其他元素
127.0.0.1:6379> GEORADIUSBYMEMBER china:city beijing 1000 km withdist 
1) 1) "beijing"
   2) "0.0000"
127.0.0.1:6379> GEORADIUSBYMEMBER china:city beijing 10000 km withdist
1) 1) "guangzhou"
   2) "1917.9473"
2) 1) "shanghai"
   2) "1088.6186"
3) 1) "beijing"
```

> GEOHASH 返回一个或多个位置元素的 Geohash 表示

该命令将返回11个字符的Geohash字符串

```bash
# 将二维的经纬度转换成一维的字符串，两个字符串越接近，距离越近
127.0.0.1:6379> GEOHASH china:city beijing
1) "wx4sucu47r0"
```

> geo底层的实现其实是Zset!,我们可以通过Zset操作geo

``` bash
127.0.0.1:6379> ZRANGE china:city 0 -1 # 查询geo中的元素
1) "guangzhou"
2) "shanghai"
3) "beijing"
127.0.0.1:6379> ZREM china:city beijing # 通过zset命令移除geo中的元素
(integer) 1
127.0.0.1:6379> ZRANGE china:city 0 -1
1) "guangzhou"
2) "shanghai"
127.0.0.1:6379> 
```

### hyperloglog 

基数统计！redis2.8.9就更新了hyperloglog数据结构！

redis hyperloglog 基数统计法

优点：占用的内存是固定的，2^64不同元素的基数，只需要12kb内存

场景：网页的uv（一个人访问网站多次，但是只能算一个用户访问）

0.81%的出错率，统计uv可以忽略不计！

```bash
127.0.0.1:6379> PFADD mykey 1 2 3 4 5 6 7 # 创建第一组数据
(integer) 1
127.0.0.1:6379> PFCOUNT mykey
(integer) 7
127.0.0.1:6379> pfadd mykey1 6 7 8 9 # 创建第二组数据
(integer) 1
127.0.0.1:6379> PFMERGE mykey2 mykey mykey1 # 合并两组 mykey mykey1 => mykey2 相当于set中取并集
OK
127.0.0.1:6379> PFCOUNT mykey2  # 取基数
(integer) 9
127.0.0.1:6379> 
```

> 什么是基数？

不重复的数！

### Bitmaps

> 位存储

0 1 0 0 0 1：疫情分感染的和未感染的，用户活跃/分活跃，打卡365天打卡正常和异常

只有两个状态都可以使用bitmaps位图，数据结构，都是操作二进制位来进行记录，只有0和1两个状态。

```bash
# 通过bitmaps设置一周的打卡情况 0未打卡 1打卡
127.0.0.1:6379> SETBIT sign 0 1 # 设置周一为打卡
(integer) 0
127.0.0.1:6379> SETBIT sign 1 1 # 设置周二为打卡
(integer) 0
127.0.0.1:6379> SETBIT sign 2 0
(integer) 0
127.0.0.1:6379> SETBIT sign 3 1
(integer) 0
127.0.0.1:6379> SETBIT sign 4 1
(integer) 0
127.0.0.1:6379> SETBIT sign 5 0
(integer) 0
127.0.0.1:6379> SETBIT sign 6 0
(integer) 0
127.0.0.1:6379> GETBIT sign 1 # 查看周一是否打卡
(integer) 1
127.0.0.1:6379> BITCOUNT sign # 统计一周的打卡情况
(integer) 4 # 打卡四天
```

### 事务

ACID:要么成功 要么失败，redis单条命令是保证原子性的，但是redis是不保证原子性的！

redis事务的本质是一组命令的集合，一个事务中的所有命令都会被序列化，在事务执行的过程中，按照顺序执行。

一次性、顺序性、排他性，执行yix系列命令！

事务没有隔离级别的概念，所有的命令在事务中，并不会直接执行，而是在发起执行命令时才会按照顺序执行！Exec

```bash
127.0.0.1:6379> MULTI  # 开启事务
OK
# 命令入队
127.0.0.1:6379(TX)> set k1 v1
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> get k1
QUEUED
127.0.0.1:6379(TX)> set k3 v3
QUEUED
# 执行事务
127.0.0.1:6379(TX)> exec
1) OK
2) OK
3) "v1"
4) OK
```

> 取消事务 discard

```BASH
127.0.0.1:6379> MULTI # 开启事务
OK
127.0.0.1:6379(TX)> set h1 v1
QUEUED
127.0.0.1:6379(TX)> set h2 v2
QUEUED
127.0.0.1:6379(TX)> get h2
QUEUED
127.0.0.1:6379(TX)> DISCARD  # 取消事务
OK
127.0.0.1:6379> exec #取消后再执行会报错
(error) ERR EXEC without MULTI 
```

> 异常1：编译型异常（代码错误，命令又错）事务中的所有命令都不会执行异常2：

``` bash
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379(TX)> set k1 v1
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> GETSET k3
(error) ERR wrong number of arguments for 'getset' command
127.0.0.1:6379(TX)> set k4 v4
QUEUED
127.0.0.1:6379(TX)> exec
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> get k4
(nil)
127.0.0.1:6379> get k1
(nil)
127.0.0.1:6379> 
```



> 异常2：运行时异常（1/0）如果事务中存在语法性错误，那么其他命令是可以正常执行的，但是异常语法会抛出异常

```bash
127.0.0.1:6379> set k1 v1
OK
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379(TX)> INCR k1 # 会在运行时执行失败
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> set k3 v3
QUEUED
127.0.0.1:6379(TX)> exec # 虽然第一条命令执行失败了，但是其他命令会执行成功
1) (error) ERR value is not an integer or out of range
2) OK
3) OK
127.0.0.1:6379> 
```

> 监控 Watch

- 悲观锁:很悲观，认为什么时候都会出现问题，无论做什么都会加锁
- 乐观锁：很乐观，无论什么时候都认为不会出现问题，在更新的时候才去判断在这个过程中数据是否被修改过了，获取version，在更新的时候比version

> redis监控测试

``` bash
# 事务正常执行
127.0.0.1:6379> set money 100
OK
127.0.0.1:6379> set out 0
OK
127.0.0.1:6379> WATCH money # 监视money对象
OK
127.0.0.1:6379> MULTI  # 事务正常结束，在期间数据没有发生变动，正常执行成功！
OK
127.0.0.1:6379(TX)> DECRBY money 20
QUEUED
127.0.0.1:6379(TX)> INCRBY out 20
QUEUED
127.0.0.1:6379(TX)> exec
1) (integer) 80
2) (integer) 20
127.0.0.1:6379> 
```

测试redis多线程，可以当作乐观锁操作！

``` bash
127.0.0.1:6379> watch money # 监视器
OK
127.0.0.1:6379> MULTI # 开启事务
OK
127.0.0.1:6379(TX)> DECRBY money 10
QUEUED
127.0.0.1:6379(TX)> INCRBY out 10
QUEUED
127.0.0.1:6379(TX)> exec # 在执行事务前，另外一个线程更新了数据
(nil) 

# 另外一个客户端
127.0.0.1:6379> get money
"70"
127.0.0.1:6379> get money
"70"
127.0.0.1:6379> DECRBY money 10
(integer) 60

# 解决方法
127.0.0.1:6379> UNWATCH # 如果发现事务执行失败，就先解锁，放弃上次的监视
OK
127.0.0.1:6379> WATCH money  # 获取最新的值,重新监视，再次重新开启新事务
OK
127.0.0.1:6379(TX)> exec # 比较监视的值，如果值没有发生变化，就执行成功，如果失败了，就再次获取最新值，重复上面的步骤
```

### jedis

> 什么是jedis？官方推荐的java操作redis的工具中间件，

```java
/**
* redis配置文件
*/
@Configuration
public class RedisConfig {

    @Bean
    @Qualifier("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }
}

```

## redis持久化

### rdb

在主从复制中，rdb操作是用来做备用的，在从机上面，不占主机的内存。

1.save规则满足的情况下回自动触发rdb操作

2.执行flushall命令后会触发rdb操作

3.退出redis也会执行rdb操作

执行rdb操作备份就会自动生成rdb文件

> 恢复rdb文件

只需要将rdb文件放到redis的启动目录，系统就会自动扫描目录下的rdb文件，恢复数据

> 优缺点

**优点**：

- 适合大规模的数据恢复，速度也很快
- 对数据的完整性要求不高

**缺点**

- 需要一定的时间间隔进程操作，如果redis服务器意外宕机了，那么对数据的最后一次修改就无法进行持久化
- fork进程的时候，会占用一定的内存空间

在生产环境上一般dump.rdb也是临时的rdb文件，为了避免文件数据丢失，会对临时rdb文件进行备份。预防数据丢失。

### AOF (append only file)

将所有命令都复制一份，history，恢复的时候就是把这个文件中的所有命令都重写执行一遍。

**跟rdb操作一样,都是父进程fork一个子进程来完成aof操作**

> 运行描述

**aof是以日志的形式来记录每一个写操作**，将Redis执行过的所有指令都记录下来（读操作不记录，没必要）只许追加文件不可以改写文件，redis启动之初会读该文件进行数据重建，换言之就是redis重启的话会根据日志文件的内容将写指令全部执行一遍以完成数据的恢复工作。

**Aof保存的是appendonly.aof文件**

aof功能默认是不开启的，需要将appendonly修改为yes才能生效。

 如果这个aof文件中有错误，那么这个redis是启动不起来的，我们需要修复这个aof文件，redis给我们提供了一个根据叫**redis-check-aof -- fix appendonly.aof进行修复。**

> rewrite规则

如果aof文件大于64mb，就会fork一个新的子进程来完成aof操作

> 优点和缺点

优点：

1.每一次修改都同步，文件的完整性会更好

2.每秒同步一次，可能会丢失1秒的数据

3.从不同步的效率最高（策略可选）

缺点：

1.相对于数据文件来说，aof永远大于rdb，修复的数据要远大于rdb

2.aof的运行效率也要比rdb慢，所以redis默认开启的是rdb快照操作。

**扩展**

redis数据存储在缓存中，断电即消失，所以一般我们在使用redis时要进行集群搭建，主从复制，并且异地多活的策略，来保证redis中的数据在出现意外的情况下能够恢复数据。同时在使用持久化的过程中默认使用的是rdb，aof可以进行手动开启，aof数据完整性恢复的更好一些，但是效率要比rdb差很多，在恢复数据时有时还需要比较主机和从机上哪一个持久化文件更新，恢复的时候选择更新的数据进行恢复，也能避免一部分数据的丢失。



### 主从复制

只配置从库，主库不需要配置！

复制三个配置文件，修改对应的信息

1.port 

2.pid名字

3.log名字 

4.备份文件名字（dump.rdb）
