# spring cloud组件

## spring cloud项目的父pom模板

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.dong</groupId>
    <artifactId>spring-cloud-dong</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>

    <modules>
				<!-- 子项目-->
        <module>user-service</module>
        <module>order-service</module>
        <module>eureka-server8761</module>
        <module>gateway</module>
        <module>cloud-provider-payment9001</module>
        <module>cloud-provider-payment9002</module>
        <module>cloud-consumer-order80</module>
        <module>cloud-consumer-order81</module>
        <module>cloud-api-commons</module>
        <module>eureka-server8762</module>
    </modules>
    <!-- 统一管理jar包版本 -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <junit.version>4.12</junit.version>
        <log4j.version>1.2.17</log4j.version>
        <lombok.version>1.16.18</lombok.version>
        <mysql.version>5.1.47</mysql.version>
        <druid.version>1.2.8</druid.version>
        <mybatis.spring.boot.version>2.2.0</mybatis.spring.boot.version>
    </properties>

    <!-- 子模块继承之后，提供作用：锁定版本+子modlue不用写groupId和version  -->
    <!-- 只做声明，不实际引用，如果模块需要引用，需要再子模块的 <dependencies>标签下声明引入-->
    <dependencyManagement>
        <dependencies>
            <!--spring boot 2.2.2-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.2.2.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--spring cloud Hoxton.SR1-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Hoxton.SR1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--spring cloud alibaba 2.1.0.RELEASE-->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>2.1.0.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql.version}</version>
            </dependency>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis.spring.boot.version}</version>
            </dependency>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>${log4j.version}</version>
            </dependency>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
                <optional>true</optional>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <fork>true</fork>
                    <addResources>true</addResources>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```



## Eureka注册中心配置

```yaml
# Eureka服务端配置
server:
  port: 8761
spring:
  application:
    name: eurekaserver
eureka:
  instance:
    hostname: eureka8761.com
  client:
    service-url:
      # eureka集群，互相注册
      #defaultZone: http://eureka8762.com:8762/eureka/
      # eureka单机版
      defaultZzone: http://eureka8761.com:8761/eureka/
    # false表示不向注册中心注册自己
    register-with-eureka: false
    # 不需要检索服务，自己端就是注册中心，不需要拉取当前已有的注册信息。职责就是维护服务实例
    fetch-registry: false
  server:
    # false 关闭自我保护机制，保证不可用的服务被及时清理,默认是true开启的
    enable-self-preservation: false
    # 扫描失效服务的间隔时间（单位毫秒，默认是60*1000）即60秒
    eviction-interval-timer-in-ms: 2000
```

```yaml
# eureka客户端配置
server:
  port: 9001
spring:
  application:
    name: cloud-payment-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: org.gjt.mm.mysql.Driver
    url: jdbc:mysql://localhost:3306/cloud2?useUnicode=true&characterEncoding=utf-8&userSSL=false
    username: root
    password: root

eureka:
  client:
    # 是否将自己注册到注册中心默认为true
    register-with-eureka: true
    # 是否从注册中心拉取已有的注册信息，默认是true，单节点无所谓，但是集群必须设置为true 才能配合ribbon完成客户端负载均衡
    fetch-registry: true
    service-url:
      defaultZone: http://localhost:8761/eureka
      # 集群版eureka注册中心
#      defaultZone: http://eureka8761.com:8761/eureka, http://eureka8762.com:8762/eureka
  instance:
    instance-id: payment9001
    # 以IP地址注册到服务中心,访问地址显示IP地址
    prefer-ip-address: true
    # eureka客户端像服务端发送心跳的时间间隔 单位秒 默认是30秒
    lease-renewal-interval-in-seconds: 1
    # eureka服务端收到客户端最后一次心跳等待的时间上限，单位秒 默认是90秒，超时将剔除服务（如果关闭了自我保护机制那么无效服务会立即被剔除）
    lease-expiration-duration-in-seconds: 2
mybatis:
  mapper-locations: classpath:mapper/*.xml
  type-aliases-package: com.dong.springcloud.entities
```

### eureka自我保护机制

**Eureka服务端会检查最近15分钟内所有Eureka 实例正常心跳占比，如果低于85%就会触发自我保护机制。触发了保护机制，Eureka将暂时把这些失效的服务保护起来，不让其过期，但这些服务也并不是永远不会过期。Eureka在启动完成后，每隔60秒会检查一次服务健康状态，如果这些被保护起来失效的服务过一段时间后（默认90秒）还是没有恢复，就会把这些服务剔除。如果在此期间服务恢复了并且实例心跳占比高于85%时，就会自动关闭自我保护机制。**

### OpenFeign超时设置

```yaml
# 设置feign客户端超时时间（openFeign默认支持ribbon）
ribbon:
  #建立连接后从服务器读取到可用资源所用的时间
  ReadTimeout: 5000
  #建立连接所用的时间，适用于网络正常的情况下，两端建立连接所用的时间 单位ms
  ConnectTimeout: 5000
```

#### feign连接池配置 todo

#### Feign 日志增强

```java
// 注入Bean
@Bean
    public Logger.Level feignLogger() {
        // 日志级别设置 FULL 最全的 有四种，默认是NONE
        return Logger.Level.FULL;
    }
```

```yaml
# 添加日志配置
logging:
  level:
    # 设置feign接口的日志级别，可以设置具体的接口，也可以设置包下的所有接口feign日志级别
    com.dong.cloud.feign: debug
```

