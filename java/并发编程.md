# Java并发编程



## 多线程基础知识

### 线程上下文切换（Thread Context switch）

- 线程的cpu时间片用完了
- 垃圾回收
- 有更高优先级的线程需要运行、
- 线程自己调用了sleep、yield、wait、join、park、synchronized、lock等方法。

当Context Switch发生时，需要由操作系统保存当前线程的状态，并恢复另一个线程的状态。Java中对应的概念就是程序计数器
（Progrem Counter register）,它的作用是记录jvm吓一条执行指令的地址，是线程私有的。

- 状态包括程序计数器和虚拟机栈中每一个栈帧信息，例如局部变量表、操作数栈、返回地址、动态链接等。
- 频繁的进行上下文切换会影响性能。

![image-20211017175938142](/Users/dongjian/Library/Application Support/typora-user-images/image-20211017175938142.png)

![image-20211017180503145](/Users/dongjian/Library/Application Support/typora-user-images/image-20211017180503145.png)

### start和run区别



### sleep和yield区别

sleep：running --> timed waiting(阻塞)

Yield: running --> runnable（就绪状态）

**yield依然是就绪状态**，但是**sleep是进入睡眠的阻塞状态**，CPU在分配时间片的时候是不会考虑处于阻塞状态的线程的，而yield的线程知识将当前线程的CPU使用权让给其他线程，当前线程在让出后依然有机会获得CPU的调度。

### interrupt

#### 打断线程：

- 打断睡眠的线程（sLeep、wait），打断标识会自动置为false
- 打断正常运行的线程，那么打断标识就会置为true--可以利用这个去终止线程的运行

#### 两阶段终止

作用：优雅的终止线程

**错误思路**：

1. stop终止线程，会真正的终止线程，如果这个线程还持有共享资源的锁，那么当使用stop终止线程后这个锁就得不到释放，其他线程将永远无法得到锁，造成其他线程无法正常运行。
2. System.exit(int)更加暴力的终止线程，而且不仅仅会使当前线程终止，而且整个程序都会终止。

![image-20211018223442126](/Users/dongjian/Library/Application Support/typora-user-images/image-20211018223442126.png)

1. 第一阶段是在正常运行的时候调用线程的interrupt操作，这个时候打断标记会为true，可以利用这个退出循环
2. 第二阶段是在睡眠的时候调用了interrupt操作，这个时候打断标记为false，但是会抛出异常，可以在异常捕获中将打断标记设置为true，在下次循环中跳出。

### 守护线程

默认情况下，Java进程需要等待所有线程都结束后才会结束，但是守护线程是一种特殊的线程，只要其他非守护线程都结束了，即使守护线程的代码没有执行完，也会被强制结束。

使用场景：

1. 垃圾回收器GC
2. Tomcat中的Acceptor和Poller都是属于守护线程，用于接收和分发请求，所以当Tomcat接收到shutdown命令后，不会等待这两个线程执行完。

### 线程活跃性

#### 线程饥饿

定义：一个优先级比较低的线程始终得不到cpu的调度，而又不能终止，这种情景可以理解为线程饥饿。

解决线程饥饿可以使用ReentrantLock锁对象解决。

### ReentrantLock相对于synchronized的特点

- 可中断
- 可已设置超时时间
- 支持多个条件变量
- 可以设置为公平锁
- 跟synchronized一样都支持锁的重入

```java
reentrantLock.lock();
  try {
    	// 临界区
  }finally {
    // 释放锁
    reentrantLock.unLock();
  }
```

```java
/**
* 尝试获取锁，如果不带参数，如果没有拿到就立即返回false
* 如果有参数，那么如果超时时间内依然没有拿到锁就会返回false，在超时时间内拿到锁就会返回true
* 在超时时间内可以被其他线程打断
*/
tryLock()
/**
* 获取可被打断的锁，在阻塞获取锁时其他线程可以打断
* threadName.interrupt()
*/
lockInterruptibly()
```

