### 创建mysql容器

- ```shell
  # 第一条命令
  # 参数说明：
  #-p 3306:3306  将主机3306端口映射到容器3306端口
  #-e MYSQL_ROOT_PASSWORD=root     设置远程登录的root用户密码为root
  #--name zyz-name    可选，设置容器别名
  #mysql   镜像名称
  
  docker run -d -p 3306:3306 --name mymysql -e MYSQL_ROOT_PASSWORD=root  docker.io/mysql:latest
  # 回车之后返回一段字符，就是我框出来的地方，接下来的第二个命令有用到前12位字符（其实也就是起个名字）
  
  # 第二个命令（进入容器命令）
  docker exec -it 7931e5017735 /bin/sh
  # 第三条命令
  mysql -uroot -p，接着输入刚刚设置好的密码
  # 第四条命令
  alter user 'root'@'%' identified with mysql_native_password by 'root';
  ```

**提示**：Mac 本地 docker中安装的mysql密码和用户名都是root

- ![](https://tva1.sinaimg.cn/large/008i3skNly1gxmywh7covj31l40maq92.jpg)

![](https://tva1.sinaimg.cn/large/008i3skNly1gxmz24xw0yj31l40magqr.jpg)

  安装成功：

![image-20211222222049534](https://tva1.sinaimg.cn/large/008i3skNly1gxmz3zwfn9j31ia09wjs2.jpg)

## Mac docker安装zookeeper（cp）

```shell
# 1.通过docker搜索zookeeper
docker search zookeeper
# 2.拉取zookeeper最新的镜像
docker pull zookeeper
# 3.查看下载的镜像
docker images
# 4.启动镜像，设置端口映射
# 参数说明
# - p　　端口映射
#　--name　　容器实例名称
#　-d　　后台运行
#  2181　　Zookeeper客户端交互端口
#  2888　　Zookeeper集群端口
#　3888　　Zookeeper选举端口
docker run --name zk -p  2181:2181 -p 2888:2888 -p 3888:3888 --restart always -d zookeeper

# 5.进入zookeeper运行环境 edeb9aae6ede：镜像id
docker exec -it edeb9aae6ede bash
# 6.启动zookeeper客户端
./bin/zkCli.sh
# 7.查看注册的服务
ls /services
# 8.获取zookeeper中服务的注册信息 后面是服务的注册id
get /services/cloud-provider-payment9003/c218a803-cebe-41d6-9ced-a9c9e1045586

备注：：zookeeper是CP保证数据一致性，没有自我保护机制；eureka是AP，保证高可用，有自我保护机制。
```

